In order for this to function you must download the AWeber API and place it into the sites/*/libraries folder as
sites/*/libraries/aweber-api (ex. /home/vhost/mysite/htdocs/sites/all/libraries/aweber-api).

Download the AWeber API from their github repository: https://github.com/aweber/AWeber-API-PHP-Library

Place the files into the folder exactly as they are organized in the GitHub repo!