<?php

/**
 * @file
 * Provides admin settings form(s)
 */

/**
 * General settings form
 */
function commerce_aweber_settings_form($form) {

  drupal_set_title(t('AWeber Commerce settings'));
  $form = array();
  $form['commerce_aweber_api_consomer_key'] = array(
    '#markup' => t('Provide your AWeber API Information.  This can be obtained by')
  );
  $form['edgecast_cdn_intro_01'] = array(
    '#markup' => t('<p align="center"><strong>WARNING and DISCLAIMER</strong></p><p>If you are not running this site over SSL encryption you <strong><i>should be!</i></strong>  Information being saved on this page can allow an unauthorized user access to your AWeber account! You have been warned.</p>'),
  );
  $form['commerce_aweber_api_consumer_key'] = array(
    '#type' => 'textfield',
    '#size' => '150',
    '#title' => t('API Consumer Key'),
    '#description' => t('Your consumer key from AWeber.'),
    '#default_value' => variable_get('commerce_aweber_api_consumer_key', ''),
  );
  $form['commerce_aweber_api_consumer_secret'] = array(
    '#type' => 'textfield',
    '#size' => '150',
    '#title' => t('API Secret Key'),
    '#description' => t('Your consumer secret from AWeber.'),
    '#default_value' => variable_get('commerce_aweber_api_consumer_secret', ''),
  );
  $form['commerce_aweber_access_key'] = array(
    '#type' => 'textfield',
    '#size' => '150',
    '#title' => t('Account Access Key'),
    '#description' => t('Your consumer secret from AWeber.'),
    '#default_value' => variable_get('commerce_aweber_access_key', ''),
  );
  $form['commerce_aweber_access_secret'] = array(
    '#type' => 'textfield',
    '#size' => '150',
    '#title' => t('Account Access Secret'),
    '#description' => t('Your consumer secret from AWeber.'),
    '#default_value' => variable_get('commerce_aweber_access_secret', ''),
  );
  $form['commerce_aweber_account_id'] = array(
    '#type' => 'textfield',
    '#size' => '150',
    '#title' => t('Account ID'),
    '#description' => t('Your account id number.'),
    '#default_value' => variable_get('commerce_aweber_account_id', ''),
  );
  $form['commerce_aweber_account_list_id'] = array(
    '#type' => 'textfield',
    '#size' => '150',
    '#title' => t('List ID'),
    '#description' => t('Your list id number.'),
    '#default_value' => variable_get('commerce_aweber_account_list_id', ''),
  );
  return system_settings_form($form);
}
