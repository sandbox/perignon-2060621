<?php

/**
 * @file
 * Rules integration for the Commerce AWeber integration.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_aweber_rules_action_info() {
  $actions = array();
  $actions['commerce_aweber_add_to_list'] = array(
    'label' => t('Add user to an AWeber List'),
    'base' => 'commerce_aweber_rules_add_to_list',
    'parameter' => array(
      'account' => array(
        'type' => 'user',
        'label' => t('Account'),
      ),
    ),
    'group' => t('Commerce AWeber'),
  );

  return $actions;
}

function commerce_aweber_rules_add_to_list($account_current) {
  // If we received an authenticated user account...
  if (!empty($account_current)) {
    $this_user = user_load($account_current->uid);
    foreach ($this_user->field_user_address['und']['0'] as $key => $values) {
      switch ($key) {
        case 'first_name':
          if (!empty($values)) {
            $fields['firstname'] = $values;
          }
          break;
        case 'last_name':
          if (!empty($values)) {
            $fields['lastname'] = $values;
          }
          break;
      }
    }
    commerce_aweber_subscription_process($this_user, $this_user->mail, $fields);
  }
}

/**
 * Help callback for the opt-in action.
 */
function commerce_aweber_rules_add_to_list_help() {
  return t('This action allows you to add a user to a specific AWeber list');
}
